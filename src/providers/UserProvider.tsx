import {createContext} from "react";
import firebase from "firebase/app";

type UserType = {
  user: firebase.User | null | undefined
}

export const UserContext = createContext<UserType>({user: null})