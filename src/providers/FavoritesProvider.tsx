import {createContext} from "react";

type FavoritesType = {
  favorites: string[],
  addToFavorites: (recipeId: string) => void
}

export const FavoritesContext = createContext<FavoritesType>({
  favorites: [], addToFavorites: () => {}
})