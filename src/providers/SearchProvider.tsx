import React, {createContext} from "react";

export type SearchType = {
  type: "Name" | "Category" | "Ingredients" | "Random" | "Letter" | undefined,
  props: {
    letter: string | undefined,
    category: string | undefined,
    findInCategory: boolean | undefined,
    value: string | undefined,
    selectedIngredients: string[],
    findWithIngredients: boolean | undefined
  }
}

type SearchContextType = {
  search: SearchType,
  setSearch: React.Dispatch<React.SetStateAction<SearchType>>
}

export const SearchContext = createContext<SearchContextType>({
  search: {
    type: "Random",
    props: {
      letter: undefined,
      category: undefined,
      value: undefined,
      selectedIngredients: [],
      findInCategory: false,
      findWithIngredients: false
    }
  }, setSearch: () => {}
})