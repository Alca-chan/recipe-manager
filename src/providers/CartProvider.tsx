import {createContext} from "react";
import {IngredientMeasure} from "../api/types";
import {CartItem} from "../utils/firebase";

type CartType = {
  cart: CartItem[],
  addToCart: (ingredient: IngredientMeasure, recipeId: string) => void
  addAllToCart: (items: CartItem[]) => void,
  removeAllFromCart: (items: CartItem[]) => void
}

export const CartContext = createContext<CartType>({
  cart: [], addToCart: () => {}, addAllToCart: () => {}, removeAllFromCart: () => {}
})