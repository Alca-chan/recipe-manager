import {useCallback, useEffect, useRef, useState} from 'react';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import {FullRecipe, IngredientMeasure} from "../api/types";

const firebaseConfig = {
  apiKey: "AIzaSyDxdHr05uxudrwpTOLVwIq2jDdhxlPioM8",
  authDomain: "cookitchef-40724.firebaseapp.com",
  projectId: "cookitchef-40724",
  storageBucket: "cookitchef-40724.appspot.com",
  messagingSenderId: "890747762036",
  appId: "1:890747762036:web:5d9350e817ab6226b3a821",
  measurementId: "G-V2SPTCMHWW"
}


// Initialize Firebase with a "default" Firebase project, or use existing if one already exists
const defaultProject = firebase.apps.length > 0 ? firebase.app() : firebase.initializeApp(firebaseConfig)
const db = defaultProject.firestore()

export type CartItem = {
  ingredient: IngredientMeasure,
  recipeId: string
}

type UserInfo = {
  user: firebase.User | null | undefined,
  favorites: string[],
  addToFavorites: (recipeId: string) => void
  cart: CartItem[],
  addToCart: (ingredient: IngredientMeasure, recipeId: string) => void,
  addAllToCart: (items: CartItem[]) => void,
  removeAllFromCart: (items: CartItem[]) => void
}

// Hook providing logged in user information
export const useLoggedInUserInfo = () => {
  // Hold user info in state
  const [user, setUser] = useState<firebase.User | null>()
  const [favorites, setFavorites] = useState<string[]>([])
  const [cart, setCart] = useState<CartItem[]>([])

  const mounted = useRef(true)

  // Setup onAuthStateChanged once when component is mounted
  useEffect(() => {
    const unsubscribe = defaultProject.auth().onAuthStateChanged(u => setUser(u))
    return () => unsubscribe()
  }, [])

  const loadFavorites = useCallback(() => {
    if (user) {
      const recipeIds: string[] = []
      favouritesCollection
        .where("userId", "==", user.uid)
        .get()
        .then((querySnapshot) => {
          if (!mounted.current) return
          querySnapshot.docs
            .forEach((doc) => {
              const recipeId = doc.data().recipeId
              recipeIds.push(recipeId)
            })
          setFavorites(recipeIds)
        }).catch(e => console.error(e.message))
    }
  }, [user])

  const loadCart = useCallback(() => {
    if (user) {
      const items: CartItem[] = []
      cartCollection
        .where("userId", "==", user.uid)
        .get()
        .then((querySnapshot) => {
          if (!mounted.current) return
          querySnapshot.docs
            .forEach((doc) => {
              const item = doc.data().ingredient
              const measure = doc.data().quantity
              const recipeId = doc.data().recipeId
              items.push({ingredient: {ingredient: item, measurement: measure}, recipeId: recipeId})
            })
          setCart(items)
        }).catch(e => console.error(e.message))
    }
  }, [user])

  useEffect(() => {
    mounted.current = true
    loadFavorites()
    loadCart()
    return () => {
      mounted.current = false
    }
  }, [loadFavorites, loadCart])

  const addToFavorites = (recipeId: string) => {
    if (favorites !== undefined && user) {
      const newLikes = favorites.filter(like => like !== recipeId)
      if (favorites.length === newLikes.length) {
        newLikes.push(recipeId)
        addToCollection({
          userId: user.uid,
          recipeId: recipeId
        }, favouritesCollection, user.uid + recipeId)
      } else {
        deleteFromCollection(favouritesCollection, user.uid + recipeId)
      }
      setFavorites(newLikes)
    }
  }

  const addToCart = (ingredient: IngredientMeasure, recipeId: string) => {
    if (ingredient !== undefined && user) {
      //leave only items that are for different recipe or are different ingredients
      const newCart = cart.filter(item => item.recipeId !== recipeId || item.ingredient.ingredient.toLowerCase() !== ingredient.ingredient.toLowerCase())
      if (cart.length === newCart.length) {
        newCart.push({ingredient: ingredient, recipeId: recipeId})
        addToCollection({
          userId: user.uid,
          ingredient: ingredient.ingredient,
          quantity: ingredient.measurement,
          recipeId: recipeId,
        }, cartCollection, user.uid + recipeId + ingredient.ingredient)
      } else {
        deleteFromCollection(cartCollection, user.uid + recipeId + ingredient.ingredient)
      }
      setCart(newCart)
    }
  }

  const addAll = (items: CartItem[]) => {
    if (user) {
      const toAdd = items.filter(item => !cart.some(i => i.recipeId === item.recipeId && i.ingredient.ingredient.toLowerCase() === item.ingredient.ingredient.toLowerCase()))
      const newCart = [...cart, ...toAdd]
      setCart(newCart)
      toAdd.forEach(item =>
        addToCollection({
          userId: user.uid,
          ingredient: item.ingredient.ingredient,
          quantity: item.ingredient.measurement,
          recipeId: item.recipeId,
        }, cartCollection, user.uid + item.recipeId + item.ingredient.ingredient))
    }
  }

  const removeAll = (items: CartItem[]) => {
    if (user) {
      const toDelete = items.filter(item => cart.some(i => i.recipeId === item.recipeId && i.ingredient.ingredient.toLowerCase() === item.ingredient.ingredient.toLowerCase()))
      const newCart = cart.filter(item => !toDelete.some(i => i.recipeId === item.recipeId && i.ingredient.ingredient.toLowerCase() === item.ingredient.ingredient.toLowerCase()))
      setCart(newCart)
      toDelete.forEach(item =>
        deleteFromCollection(cartCollection, user.uid + item.recipeId + item.ingredient.ingredient))
    }
  }

  const userInfo: UserInfo = {
    user: user,
    favorites: favorites,
    addToFavorites: addToFavorites,
    cart: cart,
    addToCart: addToCart,
    addAllToCart: addAll,
    removeAllFromCart: removeAll
  }
  return userInfo
}

// Sign up handler
export const signUp = (email: string, password: string, displayName: string) => defaultProject.auth().createUserWithEmailAndPassword(email, password)
  .then((result) => {
    if (result == null || result.user == null) return
    result.user.updateProfile({
      displayName: displayName
    })
  })

// Sign in handler
export const signIn = (email: string, password: string) =>
  defaultProject.auth().signInWithEmailAndPassword(email, password)

// Sign out handler
export const signOut = () => defaultProject.auth().signOut()

type Like = {
  userId: string,
  recipeId: string
}

type ShoppingCart = {
  userId: string,
  ingredient: string,
  quantity: string,
  recipeId: string
}

const addToCollection = (item: any, collection: firebase.firestore.CollectionReference<typeof item>, id?: string) => {
  if (id) collection.doc(id).set(item).catch(e => console.error(e.message))
  else collection.add(item).catch(e => console.error(e.message))
}

const deleteFromCollection = (collection: firebase.firestore.CollectionReference, id: string) => {
  collection.doc(id).delete().catch(e => console.error(e.message))
}

export const favouritesCollection = db.collection("likes") as firebase.firestore.CollectionReference<Like>

export const cartCollection = db.collection("cart") as firebase.firestore.CollectionReference<ShoppingCart>

export const recipeCollection = db.collection("recipes") as firebase.firestore.CollectionReference<FullRecipe & { userId: string }>