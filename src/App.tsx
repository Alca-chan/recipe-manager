import React, {useState} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch,} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import {makeStyles} from '@material-ui/core/styles';
import {
  CircularProgress,
  CssBaseline,
  MuiThemeProvider,
  unstable_createMuiStrictModeTheme as createMuiTheme
} from '@material-ui/core';
import {Login} from "./pages/Login";
import {Recipes} from "./pages/Recipes";
import {About} from "./pages/About";
import {NotFound} from "./pages/NotFound";
import {useLoggedInUserInfo} from "./utils/firebase";
import {RecipePage} from "./pages/RecipePage";
import {ScrollToTop} from "./components/ScrollToTop";
import {NavigationBar} from "./components/NavigationBar";
import {UserContext} from "./providers/UserProvider";
import {FavoritesContext} from "./providers/FavoritesProvider";
import {Favorites} from "./pages/Favorites";
import {CartContext} from "./providers/CartProvider";
import {Cart} from "./pages/Cart";
import {NewRecipe} from "./pages/NewRecipe";
import {UserRecipes} from "./pages/UserRecipes";
import {EditRecipe} from "./pages/EditRecipe";
import {SearchContext, SearchType} from "./providers/SearchProvider";

// MUI theme override
const ourTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#FFB948',
      light: '#FFE2A5',
      dark: '#FFA303',
    },
    secondary: {
      main: '#91E64F',
      dark: '#81E236',
      light: '#D0F4B4'
    },
    background: {
      default: '#FFF8E1'
    }
  },
})

const useStyles = makeStyles(theme => ({
  content: {
    [theme.breakpoints.up('md')]: {
      marginRight: theme.spacing(10),
      marginLeft: theme.spacing(10),
    },
    [theme.breakpoints.up('lg')]: {
      marginRight: theme.spacing(15),
      marginLeft: theme.spacing(15),
    },
    [theme.breakpoints.up('xl')]: {
      marginRight: theme.spacing(25),
      marginLeft: theme.spacing(25),
    },
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(6)
  }
}))

const App = () => {
  const classes = useStyles()

  const [search, setSearch] = useState<SearchType>({
    type: "Random",
    props: {
      letter: undefined,
      category: undefined,
      findInCategory: false,
      value: undefined,
      selectedIngredients: [],
      findWithIngredients: false
    }
  })
  const {user, favorites, addToFavorites, cart, addToCart, addAllToCart, removeAllFromCart} = useLoggedInUserInfo()

  return (
    <UserContext.Provider value={{user}}>
      <SearchContext.Provider value={{search, setSearch}}>
        <FavoritesContext.Provider value={{favorites, addToFavorites}}>
          <CartContext.Provider value={{cart, addToCart, addAllToCart, removeAllFromCart}}>
            <MuiThemeProvider theme={ourTheme}>
              <Router>
                <AppBar color='primary' position='static' variant='outlined'>
                  <NavigationBar/>
                </AppBar>

                <main className='App'>
                  <CssBaseline/>
                  <div className={classes.content}>
                    {/* Wait for user session */}
                    {user === undefined ? (
                      <CircularProgress/>
                    ) : (
                      <Switch>
                        <Route path='/' exact component={Recipes}/>
                        <Route path='/recipes' exact component={Recipes}/>
                        <Route path='/newrecipe' exact component={NewRecipe}/>
                        <Route path='/editrecipe/:id' exact component={EditRecipe}/>
                        <Route path='/userrecipes' exact component={UserRecipes}/>
                        <Route path='/login' exact component={Login}/>
                        <Route path='/recipe/:id' exact component={RecipePage}/>
                        <Route path='/about' exact component={About}/>
                        <Route path='/favorites' exact component={Favorites}/>
                        <Route path='/shoppinglist' exact component={Cart}/>
                        <Route component={NotFound}/>
                      </Switch>
                    )}
                  </div>
                </main>
              </Router>
              <ScrollToTop/>
            </MuiThemeProvider>
          </CartContext.Provider>
        </FavoritesContext.Provider>
      </SearchContext.Provider>
    </UserContext.Provider>
  )
}

export default App
