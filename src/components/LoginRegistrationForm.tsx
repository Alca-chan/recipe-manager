import React, {useEffect, useState} from "react";
import {Button, Card, CardActions, CardContent, TextField} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {signIn, signUp} from "../utils/firebase";
import {makeStyles} from "@material-ui/core/styles";
import {action} from "../pages/Login";

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  card: {
    padding: theme.spacing(1)
  }
}))

export const LoginRegistrationForm = (props: { actionName: action }) => {
  const [displayName, setDisplayName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')

  useEffect(() => {
    let canceled = false
    if (canceled) return // if the component is unmounted we need to prevent setting state
    setError('')
    return () => {
      canceled = true
    }
  }, [props.actionName])

  const classes = useStyles()

  const onSubmitLogin = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    signIn(email, password).catch(err => setError(err.message))
  }

  const onSubmitRegister = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    await signUp(email, password, displayName).catch((err) => setError(err.message))
  }

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    if (props.actionName === "Login") {
      onSubmitLogin(e)
    } else {
      onSubmitRegister(e)
    }
  }

  return (
    <form noValidate autoComplete='off' onSubmit={e => onSubmit(e)}>
      <Card className={classes.card}>
        <CardContent>
          {
            props.actionName === 'Register' &&
            <TextField
                fullWidth
                id="username"
                type='username'
                label="Username"
                margin='normal'
                variant='outlined'
                value={displayName}
                onChange={e => setDisplayName(e.target.value)}
            />
          }
          <TextField
            fullWidth
            id="email"
            type='email'
            label="Email"
            margin='normal'
            variant='outlined'
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
          <TextField
            fullWidth
            id="password"
            type='password'
            label="Password"
            margin='normal'
            variant='outlined'
            onChange={e => setPassword(e.target.value)}
          />
          {
            error &&
            <Typography variant='subtitle2' align='left' color='error' component={'span'}>{error}</Typography>
          }
        </CardContent>
        <CardActions>
          <Button
            className={classes.button}
            variant='contained'
            size='large'
            color='secondary'
            type='submit'>
            {props.actionName}
          </Button>
        </CardActions>
      </Card>
    </form>
  )
}