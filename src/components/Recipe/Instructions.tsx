import React from "react";
import {fade, Paper, Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  container: {
    width: '100%'
  },
  headerCell: {
    width: '100%',
    fontSize: 18
  },
  tableHeader: {
    backgroundColor: theme.palette.secondary.light
  },
  indexCell: {
    borderWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1)
  },
  textCell: {
    borderWidth: 0,
    borderTopWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1)
  }
}))

export const Instructions = (props: { data: string[] }) => {
  const classes = useStyles()

  return (
    <Paper className={classes.container}>
      <Table>
        <TableHead>
          <TableRow className={classes.tableHeader}>
            <TableCell colSpan={2} align='center' className={classes.headerCell}>Method</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            props.data.map((step, index) =>
              <TableRow key={index}>
                <TableCell align='right' className={classes.indexCell}>{index + 1}.</TableCell>
                <TableCell className={classes.textCell}>{step}</TableCell>
              </TableRow>
            )
          }
        </TableBody>
      </Table>
    </Paper>
  )
}