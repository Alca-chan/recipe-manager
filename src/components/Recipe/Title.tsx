import {Typography} from "@material-ui/core";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({

  bigLetters: {
    display: 'inline',
    fontWeight: 500,
    fontSize: 38,
    [theme.breakpoints.up('sm')]: {
      fontSize: 42
    },
    [theme.breakpoints.up('md')]: {
      fontSize: 48
    }
  },
  smallLetters: {
    display: 'inline',
    fontWeight: 500,
    fontSize: 32,
    [theme.breakpoints.up('sm')]: {
      fontSize: 36
    },
    [theme.breakpoints.up('md')]: {
      fontSize: 42
    }
  }
}))

export const Title = (props: { title: string }) => {
  const classes = useStyles()

  const parts = props.title.toUpperCase().split(" ")

  const createTitle = (i: number) => {
    const parStart = parts[i][0] === "(" && parts[i].length > 1
    const parEnd = parts[i][parts[i].length - 1] === ")"
    return (
      <Typography className={classes.bigLetters} component='div'>
        {(i > 0 ? " " : "") + parts[i].slice(0, parStart ? 2 : 1)}
        <Typography className={classes.smallLetters} component='div'>
          {parts[i].slice(parStart ? 2 : 1, parEnd ? parts[i].length - 1 : parts[i].length)}
        </Typography>
        <Typography className={classes.bigLetters} component='div'>
          {parEnd ? ")" : ""}
        </Typography>
        {
          parts.length > i + 1 &&
          createTitle(i + 1)
        }
      </Typography>
    )
  }

  return (
    <>
      {createTitle(0)}
    </>
  )
}