import React, {useContext, useState} from "react";
import {AddShoppingCart, RemoveShoppingCart, ShoppingCart} from "@material-ui/icons";
import {IconButton} from "@material-ui/core";
import {CartContext} from "../../providers/CartProvider";
import {IngredientMeasure} from "../../api/types";

export const CartButton = (props: { ingredient: IngredientMeasure, recipeId: string }) => {
  const [isHovered, setIsHovered] = useState(false)

  const {cart, addToCart} = useContext(CartContext)

  const AddToCartIcon = () => {
    return <AddShoppingCart fontSize='small'/>
  }

  const InCartIcon = () => {
    if (!isHovered)
      return <ShoppingCart fontSize='small'/>
    else
      return <RemoveShoppingCart fontSize='small'/>
  }

  return (
    <IconButton onClick={() => addToCart(props.ingredient, props.recipeId)}
                onMouseEnter={() => setIsHovered(true)} onMouseLeave={() => setIsHovered(false)}>
      {
        cart.some(item => item.recipeId === props.recipeId && item.ingredient.ingredient.toLowerCase() === props.ingredient.ingredient.toLowerCase())
          ? <InCartIcon/>
          : <AddToCartIcon/>
      }
    </IconButton>
  )
}
