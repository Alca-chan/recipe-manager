import React, {useContext} from "react";
import {fade, IconButton, Paper, Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import {IngredientMeasure} from "../../api/types";
import {makeStyles} from "@material-ui/core/styles";
import {CartButton} from "./CartButton";
import {AddShoppingCart, RemoveShoppingCart} from "@material-ui/icons";
import {CartContext} from "../../providers/CartProvider";

const useStyles = makeStyles(theme => ({
  container: {
    width: '100%'
  },
  measureCell: {
    borderWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1)
  },
  ingredientCell: {
    borderWidth: 0,
    borderTopWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1)
  },
  actionCell: {
    padding: 0,
    borderWidth: 0,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1)
  },
  headerCellMeasure: {
    borderWidth: 0,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1),
    fontSize: 18
  },
  headerCellIngredient: {
    borderWidth: 0,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1),
    fontSize: 18
  },
  headerCellActions: {
    padding: 0,
    borderWidth: 0
  },
  tableHeader: {
    backgroundColor: theme.palette.secondary.light
  },
  shoppingCartIcon: {
    marginLeft: theme.spacing(0.25),
    marginRight: theme.spacing(0.25)
  }
}))

export const IngredientsTable = (props: { data: IngredientMeasure[], recipeId: string }) => {
  const classes = useStyles()
  const {addAllToCart, removeAllFromCart} = useContext(CartContext)

  const addAll = () => {
    const newCart = props.data.map(item => {
      return {
        ingredient: item,
        recipeId: props.recipeId
      }
    })
    addAllToCart(newCart)
  }

  const removeAll = () => {
    const items = props.data.map(item => {
      return {
        ingredient: item,
        recipeId: props.recipeId
      }
    })
    removeAllFromCart(items)
  }

  return (
    <Paper className={classes.container}>
      <Table className={classes.container}>
        <TableHead className={classes.container}>
          <TableRow className={classes.tableHeader}>
            <TableCell className={classes.headerCellMeasure} align='right'>Measure</TableCell>
            <TableCell className={classes.headerCellIngredient}>Ingredient</TableCell>
            <TableCell className={classes.headerCellActions} align='center'>
              <IconButton onClick={addAll} className={classes.shoppingCartIcon}>
                <AddShoppingCart fontSize='small'/>
              </IconButton>
              <IconButton onClick={removeAll} className={classes.shoppingCartIcon}>
                <RemoveShoppingCart fontSize='small'/>
              </IconButton>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            props.data.map((row, index) =>
              <TableRow key={index}>
                <TableCell className={classes.measureCell} align='right'>{row.measurement}</TableCell>
                <TableCell className={classes.ingredientCell}>{row.ingredient}</TableCell>
                <TableCell className={classes.actionCell} align='center'>
                  <CartButton ingredient={row} recipeId={props.recipeId}/>
                </TableCell>
              </TableRow>
            )
          }
        </TableBody>
      </Table>
    </Paper>
  )
}
