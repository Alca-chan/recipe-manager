import React from "react";
import {darken, Fab, Slide, useScrollTrigger} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {ExpandLess} from "@material-ui/icons";

const useStyles = makeStyles(theme => ({
  toTop: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    [theme.breakpoints.up('lg')]: {
      right: theme.spacing(4),
    },
    [theme.breakpoints.up('xl')]: {
      right: theme.spacing(8),
    },
    backgroundColor: theme.palette.secondary.light,
    '&:hover': {
      backgroundColor: darken(theme.palette.secondary.light, 0.035),
    }
  }
}))

export const ScrollToTop = () => {

  const classes = useStyles()

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  })

  const toTop = () => {
    window['scrollTo']({top: 0, behavior: 'smooth'})
  }

  return (
    <Slide in={trigger} direction='up'>
      <Fab className={classes.toTop} onClick={toTop}><ExpandLess fontSize='large'/></Fab>
    </Slide>
  )
}