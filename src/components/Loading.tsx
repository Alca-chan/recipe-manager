import React from "react";
import {CircularProgress} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  loading: {
    marginTop: theme.spacing(5)
  }
}))

export const Loading = () => {

  const classes = useStyles()

  return (
    <CircularProgress className={classes.loading}/>
  )
}