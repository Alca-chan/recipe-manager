import {Badge, createStyles, darken, fade, Grid, IconButton, Theme, withStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import HomeRoundedIcon from "@material-ui/icons/HomeRounded";
import Button from "@material-ui/core/Button";
import {signOut} from "../utils/firebase";
import Toolbar from "@material-ui/core/Toolbar";
import React, {useContext} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {UserContext} from "../providers/UserProvider";
import {FavoritesContext} from "../providers/FavoritesProvider";
import red from "@material-ui/core/colors/red";
import {Favorite, ShoppingCart, ShoppingCartOutlined} from "@material-ui/icons";
import {CartContext} from "../providers/CartProvider";

const StyledBadge = withStyles((theme: Theme) =>
  createStyles({
    badge: {
      right: -5,
      top: 22,
      backgroundColor: theme.palette.primary.light
    }
  })
)(Badge);

const useStyles = makeStyles(theme => ({
  menuButton: {
    marginRight: theme.spacing(0.5),
    marginLeft: theme.spacing(0.5),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    [theme.breakpoints.up('sm')]: {
      marginBottom: theme.spacing(0),
      marginTop: theme.spacing(0)
    }
  },
  logoutButton: {
    marginRight: theme.spacing(0.5),
    marginLeft: theme.spacing(0.5),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    [theme.breakpoints.up('sm')]: {
      marginBottom: theme.spacing(0),
      marginTop: theme.spacing(0)
    },
    backgroundColor: theme.palette.primary.light,
    '&:hover': {
      backgroundColor: darken(theme.palette.primary.light, 0.035)
    }
  },
  favoriteButton: {
    marginRight: theme.spacing(1.5),
    '&:hover': {
      color: red[400]
    }
  },
  cartButton: {
    marginRight: theme.spacing(5),
    '&:hover': {
      color: red[400]
    }
  },
  homeButton: {
    marginRight: theme.spacing(1)
  },
  homeButtonIcon: {
    marginRight: theme.spacing(1),
    color: fade(theme.palette.common.black, 0.545)
  },
  link: {
    textDecoration: 'none'
  },
  badge: {
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'left',
    },
  }
}))

export const NavigationBar = () => {
  const classes = useStyles()

  const {user} = useContext(UserContext)
  const {favorites} = useContext(FavoritesContext)
  const {cart} = useContext(CartContext)

  return (
    <Toolbar>
      <Grid container justify='space-between' alignItems='center'>
        <Grid item>
          <Link className={classes.link} to='/'>
            <Button className={classes.homeButton} size='large'>
              <HomeRoundedIcon className={classes.homeButtonIcon}/>
              Recipes
            </Button>
          </Link>
          <Link className={classes.link} to='/about'>
            <Button className={classes.menuButton} size='large'>
              About
            </Button>
          </Link>
        </Grid>
        <Grid item>
          {
            !user
              ?
              <Link className={classes.link} to='/login'>
                <Button className={classes.menuButton} color='secondary' variant='contained' size='large'>
                  Login
                </Button>
              </Link>
              :
              <>
                <Link className={classes.link} to='/newrecipe'>
                  <Button className={classes.menuButton} size='large'>
                    New recipe
                  </Button>
                </Link>
                <Link className={classes.link} to='/userrecipes'>
                  <Button className={classes.menuButton} size='large'>
                    My recipes
                  </Button>
                </Link>
                <Link className={classes.link} to='/favorites'>
                  <IconButton className={classes.favoriteButton}>
                    <StyledBadge badgeContent={favorites.length} max={99}>
                      <Favorite/>
                    </StyledBadge>
                  </IconButton>
                </Link>
                <Link className={classes.link} to='/shoppinglist'>
                  <IconButton className={classes.cartButton}>
                    {
                      cart.length > 0
                        ?
                        <StyledBadge badgeContent={cart.length} max={99}>
                          <ShoppingCart/>
                        </StyledBadge>
                        :
                        <ShoppingCartOutlined/>
                    }
                  </IconButton>
                </Link>
                <Button className={classes.logoutButton} color='secondary' variant='contained' size='large'
                        onClick={signOut}>
                  Logout
                </Button>
              </>
          }
        </Grid>
      </Grid>
    </Toolbar>
  )
}