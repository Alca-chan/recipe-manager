import React, {useContext, useState} from "react";
import {SearchBar} from "../SearchBar";
import {RecipesContainingString} from "./RecipesContainingString";
import {SearchContext} from "../../../providers/SearchProvider";

export const SearchByName = () => {
  const {search, setSearch} = useContext(SearchContext)
  const [searchValue, setSearchValue] = useState(search.props.value ?? "")

  const onChange = (string: string) => {
    setSearchValue(string)
    const newProps = search.props
    newProps.value = string
    setSearch({type: search.type, props: newProps})
  }

  return (
    <div>
      <SearchBar onChange={onChange} initialValue={search.props.value ?? ""}/>
      {
        searchValue.length > 1 &&
        <RecipesContainingString string={searchValue}/>
      }
    </div>
  )
}