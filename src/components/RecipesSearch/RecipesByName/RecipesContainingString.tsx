import React, {useCallback, useEffect, useRef, useState} from "react";
import {Recipe} from "../../../api/types";
import {getRecipeByName} from "../../../api/api";
import {RecipesGrid} from "../RecipesGrid";
import {Loading} from "../../Loading";

export const RecipesContainingString = (props: { string: string }) => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const [data, setData] = useState<Recipe[]>()

  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      const recipes = await getRecipeByName(props.string)
      if (mounted.current) setData(recipes)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [setLoading, props.string])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  if (loading || data == null) {
    return <Loading/>
  }

  return (
    <div>
      {
        data.length > 0 &&
        <RecipesGrid data={data}/>
      }
    </div>
  )
}