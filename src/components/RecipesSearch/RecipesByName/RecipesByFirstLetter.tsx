import React, {useCallback, useContext, useEffect, useRef, useState} from "react";
import {Recipe} from "../../../api/types";
import {getRecipeByFirstLetter} from "../../../api/api";
import {RecipesGrid} from "../RecipesGrid";
import {Link, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {NoResultsText} from "../../NoResultsText";
import {SearchContext} from "../../../providers/SearchProvider";
import {Loading} from "../../Loading";

const useStyles = makeStyles(theme => ({
  container: {
    flexDirection: 'column'
  },
  alphabet: {
    marginTop: theme.spacing(2)
  },
  letter: {
    [theme.breakpoints.up('xs')]: {
      paddingRight: theme.spacing(2),
      paddingLeft: theme.spacing(2),
      marginTop: theme.spacing(3)
    },
    [theme.breakpoints.up('sm')]: {
      paddingRight: theme.spacing(1),
      paddingLeft: theme.spacing(1)
    },
    display: 'inline-block',
    cursor: 'pointer',
    color: theme.palette.primary.dark,
    fontSize: 18
  },
  selectedLetter: {
    [theme.breakpoints.up('xs')]: {
      paddingRight: theme.spacing(2),
      paddingLeft: theme.spacing(2),
      marginTop: theme.spacing(3)
    },
    [theme.breakpoints.up('sm')]: {
      paddingRight: theme.spacing(1),
      paddingLeft: theme.spacing(1)
    },
    display: 'inline-block',
    cursor: 'pointer',
    color: theme.palette.secondary.dark,
    fontWeight: 600,
    fontSize: 18
  }
}))

export const RecipesByFirstLetter = () => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const {search, setSearch} = useContext(SearchContext)
  const [letter, setLetter] = useState(search.props.letter ?? "A")
  const [data, setData] = useState<Recipe[]>()

  const classes = useStyles()

  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      let recipes = await getRecipeByFirstLetter(letter)
      if (mounted.current) setData(recipes)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [setLoading, letter])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  if (loading || data == null) {
    return <Loading/>
  }

  const letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

  const handleClick = (l: string) => {
    setLetter(l)
    const newProps = search.props
    newProps.letter = l
    setSearch({type: "Letter", props: newProps})
  }

  return (
    <div className={classes.container}>
      <div className={classes.alphabet}>
        {
          letters.map((l) =>
            <Link key={l} onClick={() => handleClick(l)}>
              <Typography className={l === letter ? classes.selectedLetter : classes.letter}>{l}</Typography>
            </Link>)
        }
      </div>
      {
        data.length < 1
          ?
          <NoResultsText>No recipes that start with {letter}</NoResultsText>
          : <RecipesGrid data={data}/>
      }
    </div>
  )
}