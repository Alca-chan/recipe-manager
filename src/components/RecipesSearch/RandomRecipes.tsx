import React, {useCallback, useEffect, useRef, useState} from "react";
import {Recipe} from "../../api/types";
import {getRandomRecipe} from "../../api/api";
import {RecipesGrid} from "./RecipesGrid";
import {Loading} from "../Loading";

export const RandomRecipes = () => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const [data, setData] = useState<Recipe[]>()

  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      const recipes: Recipe[] = []
      for (let i = 0; i < 12; i++) {
        let recipe = await getRandomRecipe()
        recipes.push(recipe)
      }
      const unique: Recipe[] = []
      recipes.forEach((item) => {
        if (!unique.some(i => i.id === item.id)) {
          unique.push(item)
        }
      })
      if (mounted.current) setData(unique)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [setLoading])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  if (loading || data == null) {
    return <Loading/>
  }

  return (
    <RecipesGrid data={data}/>
  )
}