import React, {useState} from "react";
import {Button, Card, darken, fade, IconButton, InputBase, Zoom} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import {Clear} from "@material-ui/icons";

const useStyles = makeStyles(theme => ({
  searchGrid: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%'
  },
  searchGridColor: {
    display: 'flex',
    [theme.breakpoints.up('sm')]: {
      width: '480px'
    },
    padding: theme.spacing(1),
    background: theme.palette.secondary.light
  },
  search: {
    width: '100%',
    backgroundColor: fade(theme.palette.common.white, 0.55),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.65),
    },
    display: 'flex',
    alignItems: 'center'
  },
  searchButton: {
    marginLeft: theme.spacing(1),
    height: '100%',
    backgroundColor: theme.palette.secondary.main,
    '&:hover': {
      backgroundColor: darken(theme.palette.secondary.main, 0.035),
    }
  },
  inputRoot: {
    width: '100%'
  },
  inputInput: {
    margin: theme.spacing(1),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    }
  },
  clearButton: {
    paddingRight: theme.spacing(1)
  }
}))

export const SearchBar = (props: { onChange: (string: string) => void, initialValue: string }) => {
  const [value, setValue] = useState(props.initialValue)
  const classes = useStyles()

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setValue(e.target.value)
    props.onChange(e.target.value)
  }

  const onClear = () => {
    setValue("")
    props.onChange("")
  }

  return (
    <div className={classes.searchGrid}>
      <Card className={classes.searchGridColor}>
        <div className={classes.search}>
          <InputBase
            placeholder="Search recipes"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            value={value}
            onChange={(e) => onChange(e)}
          />
          <Zoom in={value.length > 0}>
            <div className={classes.clearButton}>
              <IconButton onClick={onClear}>
                <Clear fontSize='small'/>
              </IconButton>
            </div>
          </Zoom>
        </div>
        <div>
          <Button className={classes.searchButton}>
            <SearchIcon/>
          </Button>
        </div>
      </Card>
    </div>
  )
}