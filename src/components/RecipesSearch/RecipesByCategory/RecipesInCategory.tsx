import React, {useCallback, useContext, useEffect, useRef, useState} from "react";
import {Recipe} from "../../../api/types";
import {getRecipesFromCategory} from "../../../api/api";
import {Button, fade, Typography} from "@material-ui/core";
import {RecipesGrid} from "../RecipesGrid";
import {makeStyles} from "@material-ui/core/styles";
import {NoResultsText} from "../../NoResultsText";
import {ArrowBack} from "@material-ui/icons";
import {SearchContext} from "../../../providers/SearchProvider";
import {Loading} from "../../Loading";

const useStyles = makeStyles(theme => ({
  title: {
    marginTop: theme.spacing(4),
    color: theme.palette.primary.main,
  },
  icon: {
    marginRight: theme.spacing(1)
  },
  topButtonsGrid: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(0),
    width: '100%',
    display: 'flex',
    alignItems: 'flex-start'
  },
  backButton: {
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.10),
    }
  },
}))

export const RecipesInCategory = (props: { category: string }) => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const [data, setData] = useState<Recipe[]>()
  const {search, setSearch} = useContext(SearchContext)

  const classes = useStyles()

  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      let recipes = await getRecipesFromCategory(props.category)
      if (mounted.current) setData(recipes)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [setLoading, props.category])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  const back = () => {
    const newProps = search.props
    newProps.findInCategory = false
    newProps.category = undefined
    setSearch({type: "Category", props: newProps})
  }

  if (loading || data == null) {
    return <Loading/>
  }

  if (data.length < 1) {
    return (
      <NoResultsText>No recipes in this category</NoResultsText>
    )
  }

  return (
    <>
      <div className={classes.topButtonsGrid}>
        <Button className={classes.backButton} onClick={back}>
          <ArrowBack className={classes.icon}/>Back to category selection
        </Button>
      </div>
      <Typography className={classes.title} variant='h3'>{props.category}</Typography>
      <RecipesGrid data={data}/>
    </>
  )
}