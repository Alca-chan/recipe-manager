import React from "react";
import {Card, CardActionArea, CardContent, CardHeader, Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {Category} from "../../../api/types";
import {ImageView} from "../ImageView";

const useStyles = makeStyles(theme => ({
  cardGrid: {
    marginTop: theme.spacing(5)
  },
  card: {
    background: theme.palette.primary.light
  },
  cardContent: {
    display: 'flex',
    justifyContent: 'center'
  },
  cardHeader: {
    background: theme.palette.primary.main,
  }
}))

export const CategoriesGrid = (props: { data: Category[], onClick: (categoryName: string) => void }) => {
  const classes = useStyles()

  return (
    <Grid container spacing={2} className={classes.cardGrid}>
      {
        props.data.map((category) => {
          return <Grid item xs={6} md={4} lg={3} xl={2} key={category.name}>
            <Card className={classes.card}>
              <CardActionArea onClick={() => props.onClick(category.name)}>
                <CardContent className={classes.cardContent}>
                  <ImageView imgUrl={category.imgUrl} width={'100%'}/>
                </CardContent>
                <CardHeader title={category.name} className={classes.cardHeader}/>
              </CardActionArea>
            </Card>
          </Grid>
        })
      }
    </Grid>
  )
}