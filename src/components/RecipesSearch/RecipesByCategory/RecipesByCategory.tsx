import React, {useCallback, useContext, useEffect, useRef, useState} from "react";
import {Category} from "../../../api/types";
import {getCategories} from "../../../api/api";
import {CategoriesGrid} from "./CaregoriesGrid";
import {RecipesInCategory} from "./RecipesInCategory";
import {NoResultsText} from "../../NoResultsText";
import {SearchContext} from "../../../providers/SearchProvider";
import {Loading} from "../../Loading";

export const RecipesByCategory = () => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(false)
  const [categories, setCategories] = useState<Category[]>()
  const {search, setSearch} = useContext(SearchContext)

  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      let categories = await getCategories()
      if (mounted.current) setCategories(categories)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [setLoading])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  if (loading || categories == null) {
    return <Loading/>
  }

  if (categories.length < 1) {
    return (
      <NoResultsText>No categories</NoResultsText>
    )
  }

  const handleClick = (categoryName: string) => {
    const newProps = search.props
    newProps.findInCategory = true
    newProps.category = categoryName
    setSearch({type: "Category", props: newProps})
  }

  return (
    <>
      {
        search.props.category === undefined
          ? <CategoriesGrid data={categories} onClick={handleClick}/>
          : <RecipesInCategory category={search.props.category}/>
      }
    </>
  )
}