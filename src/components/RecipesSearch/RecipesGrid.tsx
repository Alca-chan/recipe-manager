import React, {useContext} from 'react';
import {Card, CardActionArea, CardHeader, CardMedia, fade, Grid, IconButton} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {Recipe} from '../../api/types';
import FavoriteIcon from '@material-ui/icons/Favorite';
import red from '@material-ui/core/colors/red';
import {Link} from "react-router-dom";
import {ImageView} from "./ImageView";
import {UserContext} from "../../providers/UserProvider";
import {FavoritesContext} from "../../providers/FavoritesProvider";

const useStyles = makeStyles(theme => ({
  cardGrid: {
    marginTop: theme.spacing(5),
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  card: {
    background: theme.palette.primary.light,
    width: '200px',
    height: '100%',
    position: 'relative'
  },
  cardActionArea: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardContent: {
    display: 'flex',
    width: '100%'
  },
  cardHeader: {
    flex: 1,
    width: '100%'
  },
  like: {
    position: 'absolute',
    top: theme.spacing(1),
    right: theme.spacing(1),
    backgroundColor: fade(theme.palette.common.white, 0.80),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.80),
      color: red[400]
    }
  },
  likeSelected: {
    position: 'absolute',
    top: theme.spacing(1),
    right: theme.spacing(1),
    backgroundColor: fade(red[100], 0.80),
    color: red[400],
    '&:hover': {
      backgroundColor: fade(red[100], 0.80),
      color: theme.palette.text.secondary,
    }
  },
  link: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  }
}))

export const RecipesGrid = (props: { data: Recipe[] }) => {
  const classes = useStyles()
  const {user} = useContext(UserContext)
  const {favorites, addToFavorites} = useContext(FavoritesContext)

  const onLikeClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, recipeId: string) => {
    e.stopPropagation()
    addToFavorites(recipeId)
  }

  return (
    <Grid container spacing={2} className={classes.cardGrid}>
      {
        props.data.map((recipe) =>
          <Grid item key={recipe.id}>
            <Card className={classes.card}>
              <Link to={"/recipe/" + recipe.id} className={classes.link}>
                <CardActionArea className={classes.cardActionArea}>
                  <CardMedia className={classes.cardContent}>
                    <ImageView imgUrl={recipe.imgUrl + (recipe.id.includes("-") ? "" : "/preview")} width={'100%'}/>
                  </CardMedia>
                  <CardHeader title={recipe.name} className={classes.cardHeader}/>
                </CardActionArea>
              </Link>
              {
                user &&
                <IconButton
                    className={favorites?.some(like => like === recipe.id) ? classes.likeSelected : classes.like}
                    onClick={e => onLikeClick(e, recipe.id)}>
                    <FavoriteIcon/>
                </IconButton>
              }
            </Card>
          </Grid>
        )
      }
    </Grid>
  )
}