import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Grid} from "@material-ui/core";
import {Ingredient} from "../../../api/types";
import {ExpandMore} from "@material-ui/icons";
import {DividerWithIconButton} from "../../Dividers/DividerWithIconButton";
import {IngredientItem} from "./IngredientItem";

const useStyles = makeStyles(theme => ({
  container: {
    width: '100%'
  },
  cardGrid: {
    marginTop: theme.spacing(2)
  },
  divider: {
    marginTop: theme.spacing(2)
  }
}))

export const IngredientsGrid = (props: { data: Ingredient[], selected: string[], onSelect: (name: string) => void }) => {
  const initSize = 48
  const [numberOfItems, setNumberOfItems] = useState<number>(initSize)

  const classes = useStyles()

  const more = () => {
    setNumberOfItems(numberOfItems + initSize)
  }

  return (
    <div className={classes.container}>
      <Grid container spacing={2} className={classes.cardGrid}>
        {
          props.data.slice(0, numberOfItems).map((ingredient) =>
            <Grid item xs={12} sm={6} md={4} lg={3} xl={2} key={ingredient.id}>
              <IngredientItem key={ingredient.id} ingredient={ingredient.name}
                              isSelected={props.selected.some(n => n === ingredient.name)}
                              onClick={props.onSelect}/>
            </Grid>
          )
        }
      </Grid>
      <div className={classes.divider}>
        <DividerWithIconButton displayIcon={numberOfItems < props.data.length} onClick={more}>
          <ExpandMore fontSize='large'/>
        </DividerWithIconButton>
      </div>
    </div>
  )
}