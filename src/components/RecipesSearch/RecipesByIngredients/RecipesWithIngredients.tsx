import React, {useCallback, useEffect, useRef, useState} from "react";
import {Recipe} from "../../../api/types";
import {getRecipeByIngredient} from "../../../api/api";
import {RecipesGrid} from "../RecipesGrid";
import {NoResultsText} from "../../NoResultsText";
import {Loading} from "../../Loading";

export const RecipesWithIngredients = (props: { names: string[] }) => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const [data, setData] = useState<Recipe[]>()

  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      let recipes: Recipe[] = []
      for (let i = 0; i < props.names.length; i++) {
        const newRecipes = await getRecipeByIngredient(props.names[i])
        const unique = newRecipes.filter((recipe) => !recipes.some(r => r.id === recipe.id))
        recipes.push(...unique)
      }
      if (mounted.current) setData(recipes)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [setLoading, props.names])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  if (loading || data == null) {
    return <Loading/>
  }

  if (data.length < 1) {
    return (
      <NoResultsText>No recipes with selected ingredients</NoResultsText>
    )
  }

  return (
    <RecipesGrid data={data}/>
  )
}