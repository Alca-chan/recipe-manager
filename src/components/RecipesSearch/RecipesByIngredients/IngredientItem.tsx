import React from "react";
import {Card, CardActionArea, CardContent, Typography} from "@material-ui/core";
import {CheckBox, CheckBoxOutlineBlank} from "@material-ui/icons";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  card: {
    background: theme.palette.primary.light,
    height: '100%'
  },
  cardSelected: {
    background: theme.palette.secondary.light,
    height: '100%'
  },
  actionArea: {
    height: '100%'
  },
  text: {
    fontSize: 18
  },
  cardContent: {
    display: 'flex',
    alignItems: 'center'
  },
  checkboxBlank: {
    marginRight: theme.spacing(1),
    color: theme.palette.primary.dark
  },
  checkboxSelected: {
    marginRight: theme.spacing(1),
    color: theme.palette.secondary.dark
  }
}))

export const IngredientItem = (props: { ingredient: string, isSelected: boolean, onClick: (name: string) => void }) => {
  const classes = useStyles()

  const handleClick = () => props.onClick(props.ingredient)

  return (
    <Card className={props.isSelected ? classes.cardSelected : classes.card}>
      <CardActionArea className={classes.actionArea} onClick={handleClick}>
        <CardContent className={classes.cardContent}>
          {
            props.isSelected
              ? <CheckBox className={classes.checkboxSelected}/>
              : <CheckBoxOutlineBlank className={classes.checkboxBlank}/>
          }
          <Typography align='left' className={classes.text}>{props.ingredient}</Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}