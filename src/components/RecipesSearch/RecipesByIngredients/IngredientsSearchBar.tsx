import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import {IconButton, Zoom} from "@material-ui/core";
import InputAdornment from "@material-ui/core/InputAdornment";
import {Clear} from "@material-ui/icons";

export const IngredientsSearchBar = (props: { onChange: (value: string) => void, initialValue: string }) => {
  const [value, setValue] = useState(props.initialValue)

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setValue(e.target.value)
    props.onChange(e.target.value)
  }

  const onClear = () => {
    setValue("")
    props.onChange("")
  }

  return (
    <TextField
      fullWidth
      id="ingredient-search"
      label="Search ingredient"
      type='text'
      variant='outlined'
      margin='normal'
      value={value}
      onChange={(e) => onChange(e)}
      InputProps={{
        endAdornment: (
          <InputAdornment position='end'>
            <Zoom in={value !== ""}>
              <IconButton onClick={onClear}>
                <Clear fontSize='small'/>
              </IconButton>
            </Zoom>
          </InputAdornment>
        )
      }}
    />
  )
}