import React, {useState} from "react";
import {Card, CardActionArea, CardContent, Collapse, fade, Grid, lighten, Typography} from "@material-ui/core";
import {Clear, ExpandLess, ExpandMore} from "@material-ui/icons";
import {DividerWithIconButton} from "../../Dividers/DividerWithIconButton";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  cardGrid: {
    marginTop: theme.spacing(2)
  },
  cardContent: {
    display: 'flex'
  },
  card: {
    background: theme.palette.secondary.light,
    height: '100%'
  },
  cardCollapsed: {
    background: 'linear-gradient(#D0F4B4 35%, transparent 86%)',
    height: '100%'
  },
  clear: {
    marginRight: theme.spacing(1),
    color: fade(theme.palette.text.primary, 0.3)
  },
  text: {
    fontSize: 18,
    color: lighten(theme.palette.text.primary, 0.1)
  }
}))

export const SelectedIngredientsGrid = (props: { data: string[], onClick: (name: string) => void }) => {
  const [isCollapsed, setIsCollapsed] = useState<boolean>(true)

  const classes = useStyles()

  const selected = props.data

  const collapse = () => setIsCollapsed(!isCollapsed)

  return (
    <>
      <Collapse in={!isCollapsed} collapsedHeight={70}>
        <Grid container spacing={1} justify='center' alignItems='stretch' className={classes.cardGrid}>
          {
            selected.map((ingredient) =>
              <Grid item key={ingredient}>
                <Card className={isCollapsed ? classes.cardCollapsed : classes.card}>
                  <CardActionArea onClick={() => {
                    props.onClick(ingredient)
                  }}>
                    <CardContent className={classes.cardContent}>
                      <Clear className={classes.clear}/>
                      <Typography className={classes.text}>{ingredient}</Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            )
          }
        </Grid>
      </Collapse>
      <DividerWithIconButton displayIcon={true} onClick={collapse}>
        {
          isCollapsed
            ? <ExpandMore fontSize='small'/>
            : <ExpandLess fontSize='small'/>
        }
      </DividerWithIconButton>
    </>
  )
}