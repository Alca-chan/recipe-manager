import React, {useCallback, useContext, useEffect, useRef, useState} from "react";
import {Ingredient} from "../../../api/types";
import {getIngredients} from "../../../api/api";
import {Button, darken, fade, Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {IngredientsGrid} from "./IngredientsGrid";
import {ArrowBack, Clear, Search} from "@material-ui/icons";
import {IngredientsSearchBar} from "./IngredientsSearchBar";
import {SelectedIngredientsGrid} from "./SelectedIngredientsGrid";
import {RecipesWithIngredients} from "./RecipesWithIngredients";
import {NoResultsText} from "../../NoResultsText";
import {SearchContext} from "../../../providers/SearchProvider";
import {Loading} from "../../Loading";

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(3),
    width: '100%'
  },
  icon: {
    marginRight: theme.spacing(1)
  },
  buttonsGrid: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    width: '100%'
  },
  searchbar: {
    maxWidth: '500px'
  },
  clearAndBackButton: {
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.10),
    }
  },
  searchButton: {
    backgroundColor: theme.palette.secondary.main,
    '&:hover': {
      backgroundColor: darken(theme.palette.secondary.main, 0.035),
    }
  }
}))

export const RecipesByIngredients = () => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const {search, setSearch} = useContext(SearchContext)
  const [data, setData] = useState<Ingredient[]>()
  const [searchedIngredient, setSearchedIngredient] = useState('')

  const classes = useStyles()

  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      let ingredients = await getIngredients()
      if (mounted.current) setData(ingredients)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [setLoading])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  if (loading || data == null) {
    return <Loading/>
  }

  if (data.length < 1) {
    return (
      <NoResultsText>No ingredients</NoResultsText>
    )
  }

  const onSearch = () => {
    const newProps = search.props
    newProps.findWithIngredients = true
    setSearch({type: "Ingredients", props: newProps})
  }

  const back = () => {
    const newProps = search.props
    newProps.findWithIngredients = false
    setSearch({type: "Ingredients", props: newProps})
  }

  const clearAll = () => {
    const newProps = search.props
    newProps.selectedIngredients = []
    setSearch({type: "Ingredients", props: newProps})
  }

  const select = (name: string) => {
    const newSelected = search.props.selectedIngredients.filter(n => n !== name)
    if (search.props.selectedIngredients.length === newSelected.length) {
      newSelected.push(name)
    }
    const newProps = search.props
    newProps.selectedIngredients = newSelected
    setSearch({type: "Ingredients", props: newProps})
  }

  const onInputChange = (value: string) => {
    setSearchedIngredient(value)
  }

  return (
    <>
      <div className={classes.container}>
        <Grid container justify='space-between' alignItems='center' className={classes.buttonsGrid}>
          {
            search.props.findWithIngredients
              ?
              <Button className={classes.clearAndBackButton} onClick={back}><ArrowBack
                className={classes.icon}/>Back to ingredient selection</Button>
              :
              <>
                <Button className={classes.clearAndBackButton} onClick={clearAll}>
                  <Clear className={classes.icon}/>
                  Clear all
                </Button>
                <Button className={classes.searchButton} onClick={onSearch}>
                  <Search className={classes.icon}/>
                  Search
                </Button>
              </>
          }
        </Grid>
        {
          search.props.selectedIngredients.length > 0 &&
          <SelectedIngredientsGrid data={search.props.selectedIngredients} onClick={select}/>
        }
        {
          !search.props.findWithIngredients
            ? <>
              <div className={classes.searchbar}>
                <IngredientsSearchBar onChange={onInputChange} initialValue={searchedIngredient}/>
              </div>
              <IngredientsGrid
                data={searchedIngredient === '' ? data : data.filter(ingredient => ingredient.name.toLowerCase().includes(searchedIngredient.toLowerCase()))}
                selected={search.props.selectedIngredients} onSelect={select}/>
            </>
            : <RecipesWithIngredients names={search.props.selectedIngredients}/>
        }
      </div>
    </>
  )
}