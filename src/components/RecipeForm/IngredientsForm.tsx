import React, {useCallback, useEffect, useRef, useState} from "react";
import {Ingredient, IngredientMeasure} from "../../api/types";
import {fade, IconButton} from "@material-ui/core";
import {getIngredients} from "../../api/api";
import {NoResultsText} from "../NoResultsText";
import {makeStyles} from "@material-ui/core/styles";
import {Add} from "@material-ui/icons";
import {IngredientsFormItem} from "./IngredientsFormItem";
import {Loading} from "../Loading";

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(2),
    width: '100%'
  },
  grid: {
    marginBottom: theme.spacing(1)
  },
  plusIcon: {
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.10),
    }
  }
}))

export const IngredientsForm = (props: { ingredients: IngredientMeasure[], onChange: (ingredients: IngredientMeasure[]) => void, error: boolean }) => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const [ingredientsSelect, setIngredientsSelect] = useState<Ingredient[]>()
  const newItem: IngredientMeasure = {ingredient: "", measurement: ""}
  const [data, setData] = useState<IngredientMeasure[]>([...props.ingredients, newItem])

  const classes = useStyles()


  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      let ingredients = await getIngredients()
      if (mounted.current) setIngredientsSelect(ingredients)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [setLoading])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  if (loading || ingredientsSelect == null) {
    return <Loading/>
  }

  if (ingredientsSelect.length < 1) {
    return (
      <NoResultsText>No ingredients</NoResultsText>
    )
  }

  const onChange = (ingredientMeasure: IngredientMeasure, index: number) => {
    const newData = [...data.slice(0, index), ingredientMeasure, ...data.slice(index + 1)]
    setData(newData)
    props.onChange(newData)
  }

  const add = () => {
    setData([...data, newItem])
  }

  return (
    <div className={classes.container}>
      {
        data.map((ingredient, index) =>
          <IngredientsFormItem key={index} ingredient={ingredient} ingredientsSelect={ingredientsSelect}
                               onChange={(ingredientMeasure: IngredientMeasure) => onChange(ingredientMeasure, index)}
                               error={props.error && index === 0 ? "Add at least 1 ingredient" : undefined}
          />
        )
      }
      <IconButton onClick={add} className={classes.plusIcon} size='small'>
        <Add/>
      </IconButton>
    </div>
  )
}