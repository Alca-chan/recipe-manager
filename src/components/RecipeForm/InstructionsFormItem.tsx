import React, {useState} from "react";
import {TextField} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  textField: {
    margin: 0,
    width: '100%'
  },
  item: {
    marginBottom: theme.spacing(2)
  }
}))

export const InstructionsFormItem = (props: { index: number, step: string, onChange: (step: string) => void, error?: string }) => {
  const [value, setValue] = useState<string>(props.step)

  const classes = useStyles()

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setValue(e.target.value)
    props.onChange(e.target.value)
  }

  return (
    <div className={classes.item}>
      <TextField
        className={classes.textField}
        error={props.error !== undefined}
        multiline
        rowsMax={4}
        id="step"
        type='text'
        label={(props.index + 1) + ". step"}
        variant='outlined'
        value={value}
        helperText={props.error}
        onChange={onChange}
      />
    </div>
  )
}