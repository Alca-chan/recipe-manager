import React, {useState} from "react";
import {Ingredient, IngredientMeasure} from "../../api/types";
import {Grid, TextField} from "@material-ui/core";
import Autocomplete from '@material-ui/lab/Autocomplete'
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  grid: {
    marginBottom: theme.spacing(1)
  },
  inputField: {
    margin: 0,
    width: '100%'
  }
}))

export const IngredientsFormItem = (props: { ingredient: IngredientMeasure, ingredientsSelect: Ingredient[], onChange: (ingredientMeasure: IngredientMeasure) => void, error?: string }) => {
  const initialIngredient: Ingredient | null = props.ingredientsSelect.find(i => i.name.toLowerCase() === props.ingredient.ingredient.toLowerCase()) ?? null
  const [ingredientValue, setIngredientValue] = useState<Ingredient | null>(initialIngredient);
  const [ingredientInputValue, setIngredientInputValue] = useState("");
  const [measureValue, setMeasureValue] = useState<string>(props.ingredient.measurement)

  const classes = useStyles()

  const onChangeMeasure = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setMeasureValue(e.target.value)
    props.onChange({ingredient: ingredientValue ? ingredientValue.name : "", measurement: e.target.value})
  }

  const onChangeIngredient = (e: React.ChangeEvent<{}>, newValue: Ingredient | null) => {
    setIngredientValue(newValue)
    props.onChange({ingredient: newValue ? newValue.name : "", measurement: measureValue})
  }

  return (
    <Grid container justify='flex-start' alignItems='flex-start' spacing={2} className={classes.grid}>
      <Grid item xs={12} sm={6}>
        <TextField
          className={classes.inputField}
          id="ingredientMeasure"
          type='text'
          label="Measure"
          margin='normal'
          variant='outlined'
          value={measureValue}
          onChange={onChangeMeasure}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Autocomplete
          className={classes.inputField}
          value={ingredientValue}
          onChange={onChangeIngredient}
          inputValue={ingredientInputValue}
          onInputChange={(event: React.ChangeEvent<{}>, newInputValue: string) => {
            setIngredientInputValue(newInputValue)
          }}
          id="ingredientName"
          options={props.ingredientsSelect}
          getOptionLabel={(option) => option.name}
          renderInput={(params) =>
            <TextField {...params} label="Ingredient" variant="outlined"
                       error={props.error !== undefined}
                       helperText={props.error}/>}
        />
      </Grid>
    </Grid>

  )
}