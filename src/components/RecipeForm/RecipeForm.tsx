import React, {useCallback, useContext, useEffect, useRef, useState} from "react";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  Collapse,
  Container,
  Divider,
  fade,
  Grid,
  MenuItem,
  Paper,
  TextField,
  Typography
} from "@material-ui/core";
import {Category, FullRecipe, IngredientMeasure} from "../../api/types";
import {getCategories} from "../../api/api";
import {recipeCollection} from "../../utils/firebase";
import {UserContext} from "../../providers/UserProvider";
import {IngredientsForm} from "./IngredientsForm";
import {InstructionsForm} from "./InstructionsForm";
import {makeStyles} from "@material-ui/core/styles";
import {v4 as uuid} from 'uuid';
import {useHistory} from "react-router-dom";
import red from "@material-ui/core/colors/red";
import {CartContext} from "../../providers/CartProvider";
import {FavoritesContext} from "../../providers/FavoritesProvider";
import {ArrowBack, CheckCircleOutline} from "@material-ui/icons";
import {Loading} from "../Loading";

const useStyles = makeStyles(theme => ({
  container: {
    marginBottom: theme.spacing(3),
    width: '100%'
  },
  buttonsGrid: {
    width: '100%',
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  cancelButton: {
    marginLeft: theme.spacing(2),
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.10),
    }
  },
  deleteButton: {
    backgroundColor: fade(red[400], 0.9),
    '&:hover': {
      backgroundColor: fade(red[400], 0.95),
    }
  },
  icon: {
    marginRight: theme.spacing(1)
  },
  topButtonsGrid: {
    marginBottom: theme.spacing(2),
    display: 'flex',
    alignItems: 'flex-start'
  },
  backButton: {
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.10),
    }
  },
  successIcon: {
    marginRight: theme.spacing(1),
    color: theme.palette.secondary.dark
  },
  success: {
    display: 'flex',
    alignItems: 'flex-start',
    marginTop: theme.spacing(1.5),
    padding: theme.spacing(2),
    backgroundColor: theme.palette.secondary.light
  }
}))

export const RecipeForm = (props: { recipe?: FullRecipe }) => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)

  const [categoriesSelect, setCategoriesSelect] = useState<Category[]>()
  const {user} = useContext(UserContext)
  const {cart, removeAllFromCart} = useContext(CartContext)
  const {favorites, addToFavorites} = useContext(FavoritesContext)

  const [error, setError] = useState<boolean>(false)
  const [success, setSuccess] = useState(false)

  const recipe = props.recipe
  const [name, setName] = useState<string>(recipe ? recipe.name : "")
  const [category, setCategory] = useState<string>(recipe ? recipe.category : "")
  const [area, setArea] = useState<string>(recipe ? recipe.area : "")
  const [imgUrl, setImgUrl] = useState<string>(recipe ? recipe.imgUrl : "")
  const [youtubeUrl, setYoutubeUrl] = useState<string>(recipe ? recipe.youtubeUrl : "")
  const [ingredients, setIngredients] = useState<IngredientMeasure[]>(recipe ? recipe.ingredients : [])
  const [instructions, setInstructions] = useState<string[]>(recipe ? recipe.instructions : [])

  const history = useHistory()

  const classes = useStyles()

  const loadCategories = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      let categories = await getCategories()
      if (mounted.current) setCategoriesSelect(categories)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [setLoading])

  useEffect(() => {
    mounted.current = true
    loadCategories()
    return () => {
      mounted.current = false
    }
  }, [loadCategories])

  const onSubmit = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    const isError = name === "" || category === "" || ingredients.filter(i => i.ingredient !== "").length === 0 || instructions.length === 0
    setError(isError)
    if (!isError && user) {
      const id = recipe && recipe.id.includes("-") ? recipe.id : uuid()
      recipeCollection.doc(id).set({
        userId: user.uid,
        id: id,
        name: name,
        category: category,
        area: area,
        imgUrl: imgUrl,
        youtubeUrl: youtubeUrl,
        ingredients: ingredients.filter(ingredient => ingredient.ingredient !== ""),
        instructions: instructions.filter(instruction => instruction !== "")
      }).catch(e => console.error(e.message))
      if (recipe && recipe.id.includes("-")) {
        setSuccess(true)
        setTimeout(
          () => {
            if (mounted.current) setSuccess(false)
          },
          5000
        )
      } else {
        history.push("/recipe/" + id)
      }
    }
  }

  const cancel = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    if (recipe) {
      history.push("/recipe/" + recipe.id)
    } else {
      history.push("/")
    }
  }

  const deleteRecipe = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    if (recipe) {
      if (favorites.some(item => item === recipe.id)) {
        addToFavorites(recipe.id)
      }
      const toDelete = cart.filter(item => item.recipeId === recipe.id)
      removeAllFromCart(toDelete)
      recipeCollection.doc(recipe.id).delete().catch(e => console.error(e.message))
      history.push("/")
    }
  }

  if (loading || categoriesSelect == null) {
    return <Loading/>
  }

  const onIngredientsChange = (ingredients: IngredientMeasure[]) => {
    setIngredients(ingredients)
  }

  const onInstructionsChange = (instructions: string[]) => {
    setInstructions(instructions)
  }

  return (
    <Container maxWidth='md'>
      {
        recipe && (
          <div className={classes.topButtonsGrid}>
            <Button className={classes.backButton} onClick={cancel}>
              <ArrowBack className={classes.icon}/>Back to recipe page
            </Button>
          </div>
        )
      }
      <form noValidate autoComplete='off'>
        <Card>
          <CardContent>
            <div className={classes.container}>
              <Grid container justify='flex-start' alignItems='flex-start' spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    required
                    error={error && name === ""}
                    id="title"
                    type='text'
                    label="Title"
                    variant='outlined'
                    value={name}
                    helperText={error && name === "" ? "Name has to be filled in" : ""}
                    onChange={e => setName(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    fullWidth
                    required
                    error={error && category === ""}
                    select
                    id="category"
                    type='text'
                    label="Category"
                    variant='outlined'
                    value={category}
                    helperText={error && category === "" ? "Category has to be selected" : ""}
                    onChange={e => setCategory(e.target.value)}
                    style={{textAlign: 'left'}}
                  >
                    {
                      categoriesSelect.map((option) => (
                        <MenuItem key={option.name} value={option.name}>
                          {option.name}
                        </MenuItem>
                      ))
                    }
                  </TextField>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    fullWidth
                    id="area"
                    type='text'
                    label="Area"
                    variant='outlined'
                    value={area}
                    onChange={e => setArea(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    id="imgUrl"
                    type='url'
                    label="Image URL"
                    variant='outlined'
                    value={imgUrl}
                    onChange={e => setImgUrl(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    id="youtubeUrl"
                    type='url'
                    label="YouTube URL"
                    variant='outlined'
                    value={youtubeUrl}
                    onChange={e => setYoutubeUrl(e.target.value)}
                  />
                </Grid>
              </Grid>
            </div>
            <Divider/>
            <IngredientsForm ingredients={ingredients} onChange={onIngredientsChange}
                             error={error && ingredients.filter(i => i.ingredient !== "").length < 1}/>
            <Divider/>
            <InstructionsForm instructions={instructions} onChange={onInstructionsChange}
                              error={error && instructions.length < 1}/>
            <Divider/>
            <Collapse in={success}>
              <Paper className={classes.success}>
                <CheckCircleOutline className={classes.successIcon}/><Typography> Successfully saved</Typography>
              </Paper>
            </Collapse>
          </CardContent>
          <CardActions>
            <Grid container justify='space-between' alignItems='center' spacing={2} className={classes.buttonsGrid}>
              <Grid item>
                <Button
                  variant='contained'
                  size='large'
                  color='secondary'
                  onClick={e => onSubmit(e)}
                >
                  Save
                </Button>
                <Button
                  className={classes.cancelButton}
                  variant='contained'
                  size='large'
                  onClick={e => cancel(e)}
                >
                  Cancel
                </Button>
              </Grid>
              {
                recipe && recipe.id.includes("-") && (
                  <Grid item>
                    <Button
                      className={classes.deleteButton}
                      variant='contained'
                      size='large'
                      onClick={e => deleteRecipe(e)}
                    >
                      Delete
                    </Button>
                  </Grid>
                )
              }
            </Grid>
          </CardActions>
        </Card>
      </form>
    </Container>
  )
}