import React, {useState} from "react";
import {fade, IconButton} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {Add} from "@material-ui/icons";
import {InstructionsFormItem} from "./InstructionsFormItem";

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(2),
    width: '100%'
  },
  grid: {
    marginBottom: theme.spacing(1)
  },
  plusIcon: {
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.10),
    }
  }
}))

export const InstructionsForm = (props: { instructions: string[], onChange: (instructions: string[]) => void, error: boolean }) => {
  const newItem = ""
  const [data, setData] = useState<string[]>([...props.instructions, newItem])

  const classes = useStyles()

  const onChange = (step: string, index: number) => {
    const newData = [...data.slice(0, index), step, ...data.slice(index + 1)]
    setData(newData)
    props.onChange(newData)
  }

  const add = () => {
    setData([...data, newItem])
  }

  return (
    <div className={classes.container}>
      {
        data.map((step, index) =>
          <InstructionsFormItem key={index} index={index} step={step}
                                onChange={(step: string) => onChange(step, index)}
                                error={props.error && index === 0 ? "Add at least 1 step" : undefined}/>
        )
      }
      <IconButton onClick={add} className={classes.plusIcon} size='small'>
        <Add/>
      </IconButton>
    </div>
  )
}