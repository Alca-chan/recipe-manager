import React from "react";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  divider: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  border: {
    borderBottom: "2px solid lightgray",
    flex: 1
  }
}))

export const HorizontalDivider = (props: { children?: React.ReactNode }) => {
  const classes = useStyles()

  return (
    <div className={classes.divider}>
      <div className={classes.border}/>
      {
        props.children &&
        <>
          {props.children}
          <div className={classes.border}/>
        </>
      }
    </div>
  )
}