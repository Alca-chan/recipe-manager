import React from "react";
import {IconButton, makeStyles, Typography} from "@material-ui/core";
import {HorizontalDivider} from "./HorizontalDivider";

const useStyles = makeStyles(theme => ({
  content: {
    paddingTop: theme.spacing(0.5),
    paddingBottom: theme.spacing(0.5),
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    color: 'gray'
  }
}))

export const DividerWithIconButton = (props: { displayIcon: boolean, children: React.ReactNode, onClick?: () => void }) => {
  const classes = useStyles()

  return (
    <HorizontalDivider>
      {
        props.displayIcon &&
        <>
            <Typography variant='h5' align='center' component={'span'} className={classes.content}>
                <IconButton onClick={props.onClick}>{props.children}</IconButton>
            </Typography>
        </>
      }
    </HorizontalDivider>
  )
}