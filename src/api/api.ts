import {Category, FullRecipe, Ingredient, IngredientMeasure, Meal, Recipe} from "./types";

export const getCategories = (): Promise<Category[]> => {
  return fetchCategories().then((data) => {
    let result: Category[] = []
    data.categories?.forEach((category: { idCategory: string, strCategory: string, strCategoryThumb: string, strCategoryDescription: string }) => {
      const myCategory: Category = {
        name: category.strCategory,
        description: category.strCategoryDescription,
        imgUrl: category.strCategoryThumb
      }
      result.push(myCategory)
    })
    return result
  })
}

export const getRecipesFromCategory = (category: string): Promise<Recipe[]> => {
  return fetchRecipesFromCategory(category).then((data) => {
    let result: Recipe[] = []
    data.meals?.forEach((meal: Pick<Meal, "strMeal" | "idMeal" | "strMealThumb">) => {
      const recipe: Recipe = {
        name: meal.strMeal,
        id: meal.idMeal,
        imgUrl: meal.strMealThumb
      }
      result.push(recipe)
    })
    return result
  })
}

export const getRecipeByFirstLetter = (letter: string): Promise<Recipe[]> => {
  return fetchRecipeByFirstLetter(letter).then((data) => {
    let result: Recipe[] = []
    data.meals?.forEach((meal: Meal) => {
      const recipe: Recipe = {
        name: meal.strMeal,
        id: meal.idMeal,
        imgUrl: meal.strMealThumb
      }
      result.push(recipe)
    })
    return result
  })
}

export const getRecipeByName = (string: string): Promise<Recipe[]> => {
  return fetchRecipeByName(string).then((data) => {
    let result: Recipe[] = []
    data.meals?.forEach((meal: Meal) => {
      const recipe: Recipe = {
        name: meal.strMeal,
        id: meal.idMeal,
        imgUrl: meal.strMealThumb
      }
      result.push(recipe)
    })
    return result
  })
}

export const getRecipeById = (id: string): Promise<FullRecipe[]> => {
  return fetchRecipeById(id).then((data) => {
    let result: FullRecipe[] = []
    data.meals?.forEach((meal: Meal) => {
      const ingredients: IngredientMeasure[] = []
      for (let i = 1; i <= 20; i++) {
        const ingredient = (meal as any)["strIngredient" + i]
        const measurement = (meal as any)["strMeasure" + i]
        if (ingredient === "" || ingredient == null) break
        const ingredientWithMeasure: IngredientMeasure = {
          ingredient: ingredient,
          measurement: measurement
        }
        ingredients.push(ingredientWithMeasure)
      }

      const uniqueIngredients: IngredientMeasure[] = []
      ingredients.forEach(ingredient => {
        const match = uniqueIngredients.find(i => i.ingredient.toLowerCase() === ingredient.ingredient.toLowerCase())
        if (match === undefined) {
          uniqueIngredients.push(ingredient)
        } else {
          match.measurement = match.measurement + " + " + ingredient.measurement
        }
      })

      const instructions: string[] = meal.strInstructions
        .replace(/\.$/g, "")
        .replace(/[\n\r\t]+/g, " ")
        .replace(/(\.([A-Z]))/g, ". $2") // add whitespace after . at the end a sentence if there is none
        .replace(/(\.\) ([A-Z]))/g, ".). $2") // ensure sentence in ( ) is also split
        .replace(/\. \(/g, ".\u2008(") // we do not want to separate a sentence in ( ) from the previous sentence
        .split(". ")
        .filter(step => step.length > 2 && step !== "2 Servings 1") //some additional filters for specific recipes

      const recipe: FullRecipe = {
        name: meal.strMeal,
        id: meal.idMeal,
        imgUrl: meal.strMealThumb,
        youtubeUrl: meal.strYoutube,
        ingredients: uniqueIngredients,
        instructions: instructions,
        category: meal.strCategory,
        area: meal.strArea
      }
      result.push(recipe)
    })
    return result
  })
}

export const getIngredients = (): Promise<Ingredient[]> => {
  return fetchIngredients().then((data) => {
    let result: Ingredient[] = []
    data.meals?.forEach((ingredient: { idIngredient: string, strIngredient: string, strDescription: string }) => {
      const myIngredient: Ingredient = {
        name: ingredient.strIngredient,
        id: ingredient.idIngredient,
        description: ingredient.strDescription
      }
      result.push(myIngredient)
    })
    return result
  })
}

export const getRecipeByIngredient = (name: string): Promise<Recipe[]> => {
  return fetchRecipeByIngredient(name).then((data) => {
    let result: Recipe[] = []
    data.meals?.forEach((meal: Meal) => {
      const recipe: Recipe = {
        name: meal.strMeal,
        id: meal.idMeal,
        imgUrl: meal.strMealThumb
      }
      result.push(recipe)
    })
    return result
  })
}

export const getRandomRecipe = (): Promise<Recipe> => {
  return fetchRandomRecipe().then((data) => {
    const meal: { strMeal: string, strMealThumb: string, idMeal: string } = data.meals[0]
    const recipe: Recipe = {
      name: meal.strMeal,
      id: meal.idMeal,
      imgUrl: meal.strMealThumb
    }
    return recipe
  })
}

async function fetchCategories() {
  return await fetch("https://www.themealdb.com/api/json/v1/1/categories.php")
    .then(response => response.json())
}

async function fetchRecipesFromCategory(category: string) {
  return await fetch("https://www.themealdb.com/api/json/v1/1/filter.php?c=" + category)
    .then(response => response.json())
}

async function fetchRecipeByFirstLetter(letter: string) {
  return await fetch("https://www.themealdb.com/api/json/v1/1/search.php?f=" + letter)
    .then(response => response.json())
}

async function fetchRecipeByName(string: string) {
  return await fetch("https://www.themealdb.com/api/json/v1/1/search.php?s=" + string)
    .then(response => response.json())
}

async function fetchRecipeById(id: string) {
  return await fetch("https://www.themealdb.com/api/json/v1/1/lookup.php?i=" + id)
    .then(response => response.json())
}

async function fetchIngredients() {
  return await fetch("https://www.themealdb.com/api/json/v1/1/list.php?i=list")
    .then(response => response.json())
}

async function fetchRecipeByIngredient(name: string) {
  return await fetch("https://www.themealdb.com/api/json/v1/1/filter.php?i=" + name)
    .then(response => response.json())
}

async function fetchRandomRecipe() {
  return await fetch("https://www.themealdb.com/api/json/v1/1/random.php")
    .then(response => response.json())
}