import {Button, darken, Grid, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {DividerWithText} from "../components/Dividers/DividerWithText";
import {RecipesByCategory} from "../components/RecipesSearch/RecipesByCategory/RecipesByCategory";
import {RecipesByFirstLetter} from "../components/RecipesSearch/RecipesByName/RecipesByFirstLetter";
import {useContext, useState} from "react";
import {RandomRecipes} from "../components/RecipesSearch/RandomRecipes";
import {RecipesByIngredients} from "../components/RecipesSearch/RecipesByIngredients/RecipesByIngredients";
import {SearchByName} from "../components/RecipesSearch/RecipesByName/RecipesByName";
import {SearchContext} from "../providers/SearchProvider";

const useStyles = makeStyles(theme => ({
  container: {
    padding: theme.spacing(1),
    display: 'flex',
    width: '100%',
    justifyContent: 'center'
  },
  divider: {
    width: '100%',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5)
  },
  buttonsGrid: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.up('xs')]: {
      display: 'inline',
    },
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    }
  },
  button: {
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(0.2),
    marginLeft: theme.spacing(0.2),
    backgroundColor: theme.palette.secondary.light,
    '&:hover': {
      backgroundColor: darken(theme.palette.secondary.light, 0.035),
    }
  },
  selectedButton: {
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(0.2),
    marginLeft: theme.spacing(0.2),
    backgroundColor: theme.palette.secondary.main,
    '&:hover': {
      backgroundColor: darken(theme.palette.secondary.main, 0.035),
    }
  },
  searchByText: {
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(2),
    color: theme.palette.text.secondary
  },
  space: {
    [theme.breakpoints.up('md')]: {
      marginLeft: theme.spacing(6)
    }
  }
}))

type SearchBy = "Letter" | "Category" | "Ingredients" | "Random" | "Name"

export const Recipes = () => {
  const {search, setSearch} = useContext(SearchContext)
  const [searchBy, setSearchBy] = useState<SearchBy>(search.type ?? "Random")
  const [refresh, setRefresh] = useState(1) // random recipes button is used to get new random recipes

  const classes = useStyles()

  const choose = () => {
    switch (search.type ?? searchBy) {
      case "Category":
        return <RecipesByCategory/>
      case "Letter":
        return <RecipesByFirstLetter/>
      case "Ingredients":
        return <RecipesByIngredients/>
      case "Random":
        return <RandomRecipes key={refresh}/>
    }
  }

  const handleClick = (by: SearchBy) => {
    setSearchBy(by)
    setRefresh(refresh === 1 ? 0 : 1)
    setSearch({type: by, props: search.props})
  }

  return (
    <Grid container className={classes.container}>
      <SearchByName/>
      <div className={classes.divider}>
        <DividerWithText text="or"/>
      </div>
      <div className={classes.buttonsGrid}>
        <Typography className={classes.searchByText} variant='subtitle1'>Recipes by:</Typography>
        <Button className={searchBy === "Category" ? classes.selectedButton : classes.button}
                onClick={() => handleClick("Category")}>Category</Button>
        <Button className={searchBy === "Letter" ? classes.selectedButton : classes.button}
                onClick={() => handleClick("Letter")}>Title</Button>
        <Button className={searchBy === "Ingredients" ? classes.selectedButton : classes.button}
                onClick={() => handleClick("Ingredients")}>Ingredients</Button>
        <div className={classes.space}/>
        <Button className={searchBy === "Random" ? classes.selectedButton : classes.button}
                onClick={() => handleClick("Random")}>Random recipes</Button>
      </div>
      {choose()}
    </Grid>
  )
}