import React, {useCallback, useContext, useEffect, useRef, useState} from "react";
import {FavoritesContext} from "../providers/FavoritesProvider";
import {RecipesGrid} from "../components/RecipesSearch/RecipesGrid";
import {Recipe} from "../api/types";
import {getRecipeById} from "../api/api";
import {UserContext} from "../providers/UserProvider";
import {useHistory} from "react-router-dom";
import {NoResultsText} from "../components/NoResultsText";
import {recipeCollection} from "../utils/firebase";
import {Loading} from "../components/Loading";

export const Favorites = () => {
  const history = useHistory()
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const {user} = useContext(UserContext)
  const {favorites} = useContext(FavoritesContext)
  const [data, setData] = useState<Recipe[]>([])

  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      const recipes: Recipe[] = []
      for (let i = 0; i < favorites.length; i++) {
        if (favorites[i].includes("-")) {
          if (!user) break
          await recipeCollection
            .where("userId", "==", user.uid)
            .where("id", "==", favorites[i])
            .get()
            .then((querySnapshot) => {
              if (!mounted.current) return
              if (querySnapshot.docs.length === 1) {
                recipes.push(querySnapshot.docs[0].data() as Recipe)
              }
            })
        } else {
          let response = await getRecipeById(favorites[i])
          if (response.length > 0) {
            let recipe = response[0]
            recipes.push({id: recipe.id, name: recipe.name, imgUrl: recipe.imgUrl})
          }
        }
      }
      if (mounted.current) setData(recipes)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [user, favorites, setLoading])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  if (!user) {
    history.push("/login")
  }

  if (favorites.length < 1) {
    return <NoResultsText>Nothing in favorites</NoResultsText>
  }

  if (loading || data == null) {
    return <Loading/>
  }

  return (
    <RecipesGrid data={data}/>
  )
}