import React from 'react';
import {Card, CardContent, CardHeader, Container, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: theme.spacing(3)
  },
  header: {
    backgroundColor: theme.palette.secondary.main
  },
  content: {
    backgroundColor: theme.palette.secondary.light
  },
  divider: {
    marginTop: theme.spacing(2)
  },
  text: {
    fontSize: 18
  },
  link: {
    display: 'inline',
    textDecoration: 'none',
    color: theme.palette.text.primary
  }
}))

export const About = () => {
  const classes = useStyles()

  return (
    <Container maxWidth='sm'>
      <Card>
        <CardHeader className={classes.header} title="Cookitchef"/>
        <CardContent className={classes.content}>
          <Typography className={classes.text}>
            This application is for exploring new and managing your recipes.
          </Typography>
          <Typography className={classes.text}>
            You can add recipes to favourites and create shopping lists.
          </Typography>
          <br/>
          <Typography className={classes.text}>
            Explore, create a shopping list and cook it!
          </Typography>
        </CardContent>
      </Card>
      <Card className={classes.card}>
        <CardContent className={classes.content}>
          <Typography className={classes.text}>
            Cookitchef was created by Alena Zahradníčková in React with TypeScript using Material UI.
          </Typography>
          <Typography className={classes.text}>
            It uses <a href="https://firebase.google.com/" className={classes.link}>Firebase</a> Hosting and Firestore
            and <a href="https://www.themealdb.com/api.php" className={classes.link}>The Meal DB API</a>.
          </Typography>
        </CardContent>
      </Card>
    </Container>
  )
}