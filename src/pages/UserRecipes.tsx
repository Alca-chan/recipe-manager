import React, {useCallback, useContext, useEffect, useRef, useState} from "react";
import {useHistory} from "react-router-dom";
import {UserContext} from "../providers/UserProvider";
import {Recipe} from "../api/types";
import {NoResultsText} from "../components/NoResultsText";
import {RecipesGrid} from "../components/RecipesSearch/RecipesGrid";
import {recipeCollection} from "../utils/firebase";
import {Loading} from "../components/Loading";

export const UserRecipes = () => {
  const history = useHistory()
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const {user} = useContext(UserContext)
  const [data, setData] = useState<Recipe[]>([])

  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      if (!user) return
      const recipes: Recipe[] = []
      await recipeCollection
        .where("userId", "==", user.uid)
        .get()
        .then((querySnapshot) => {
          if (!mounted.current) return
          querySnapshot.docs
            .forEach((doc) => {
              recipes.push(doc.data() as Recipe)
            })
        })
      if (mounted.current) setData(recipes)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [user, setLoading])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  if (!user) {
    history.push("/login")
  }

  if (loading || data == null) {
    return <Loading/>
  }

  if (data.length < 1) {
    return <NoResultsText>No recipes</NoResultsText>
  }

  return (
    <RecipesGrid data={data}/>
  )
}