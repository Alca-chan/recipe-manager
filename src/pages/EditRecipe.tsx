import React, {useCallback, useContext, useEffect, useRef, useState} from 'react';
import {getRecipeById} from "../api/api";
import {FullRecipe} from "../api/types";
import {NoResultsText} from "../components/NoResultsText";
import {UserContext} from "../providers/UserProvider";
import {recipeCollection} from "../utils/firebase"
import {RecipeForm} from "../components/RecipeForm/RecipeForm";
import {Loading} from "../components/Loading";

export const EditRecipe = (props: { match: any }) => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const [data, setData] = useState<FullRecipe | null>()
  const [error, setError] = useState(false)
  const {user} = useContext(UserContext)

  const id: string = props.match.params.id

  const loadRecipeFromApi = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      let recipes = await getRecipeById(id)
      if (recipes.length < 1) {
        if (!mounted.current) return
        setData(undefined)
        setError(true)
      } else {
        if (mounted.current) setData(recipes[0])
      }
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [id, setLoading])

  const loadRecipeFromFirebase = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      if (!mounted) return
      if (!user) {
        setData(undefined)
        setError(true)
        return
      }
      await recipeCollection
        .where("userId", "==", user.uid)
        .where("id", "==", id)
        .get()
        .then((querySnapshot) => {
          if (!mounted.current) return
          const docs = querySnapshot.docs
          if (docs.length !== 1) {
            setData(undefined)
            setError(true)
            return
          }
          const recipe = docs[0].data() as FullRecipe
          if (mounted) setData(recipe)
        })
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [user, id, setLoading])

  useEffect(() => {
    mounted.current = true
    if (!id.includes("-")) {
      loadRecipeFromApi()
    } else {
      loadRecipeFromFirebase()
    }
    return () => {
      mounted.current = false
    }
  }, [id, loadRecipeFromApi, loadRecipeFromFirebase])

  if (error) {
    return (
      <NoResultsText>No recipe with id {id} found</NoResultsText>
    )
  }

  if (loading || data == null) {
    return <Loading/>
  }

  return (
    <RecipeForm recipe={data}/>
  )
}