import React, {useCallback, useContext, useEffect, useRef, useState} from "react";
import {UserContext} from "../providers/UserProvider";
import {CartContext} from "../providers/CartProvider";
import {Link, useHistory} from "react-router-dom";
import {fade, Grid, IconButton, Paper, Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {CartButton} from "../components/Recipe/CartButton";
import {Recipe} from "../api/types";
import {getRecipeById} from "../api/api";
import {NoResultsText} from "../components/NoResultsText";
import {RemoveShoppingCart} from "@material-ui/icons";
import {recipeCollection} from "../utils/firebase";
import {Loading} from "../components/Loading";

const useStyles = makeStyles(theme => ({
  container: {
    justifyContent: 'center',
    paddingRight: theme.spacing(1),
    paddingLeft: theme.spacing(1)
  },
  table: {
    width: '100%'
  },
  measureCell: {
    borderWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1)
  },
  ingredientCell: {
    borderWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1)
  },
  actionCell: {
    padding: 0,
    borderWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1)
  },
  headerCellMeasure: {
    borderWidth: 0,
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1),
    fontSize: 18
  },
  headerCellTitle: {
    borderWidth: 0,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1),
    fontSize: 18
  },
  headerCellIngredient: {
    borderWidth: 0,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1),
    fontSize: 18
  },
  headerCellActions: {
    padding: 0,
    borderWidth: 0,
    borderRightWidth: 1,
    borderStyle: 'solid',
    borderColor: fade(theme.palette.common.black, 0.1)
  },
  tableHeader: {
    backgroundColor: theme.palette.secondary.light
  },
  link: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  },
  shoppingCartIcon: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  }
}))

export const Cart = () => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(false)
  const {user} = useContext(UserContext)
  const {cart, removeAllFromCart} = useContext(CartContext)
  const history = useHistory()
  const [recipes, setRecipes] = useState<Recipe[]>([])

  const classes = useStyles()

  const loadData = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      const recipeIds: string[] = []
      cart.forEach(item => {
        if (!recipeIds.some(i => i === item.recipeId)) {
          recipeIds.push(item.recipeId)
        }
      })
      const recipes: Recipe[] = []
      for (let i = 0; i < recipeIds.length; i++) {
        if (!mounted.current) return
        if (recipeIds[i].includes("-")) {
          if (!user) break
          await recipeCollection
            .where("userId", "==", user.uid)
            .where("id", "==", recipeIds[i])
            .get()
            .then((querySnapshot) => {
              const docs = querySnapshot.docs
              if (docs.length !== 1) return
              const recipe = docs[0].data() as Recipe
              recipes.push({id: recipe.id, name: recipe.name, imgUrl: recipe.imgUrl})
            })
        } else {
          let response = await getRecipeById(recipeIds[i])
          if (response.length > 0) {
            let recipe = response[0]
            recipes.push({id: recipe.id, name: recipe.name, imgUrl: recipe.imgUrl})
          }
        }
      }
      if (mounted.current) setRecipes(recipes)
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [user, cart, setLoading])

  useEffect(() => {
    mounted.current = true
    loadData()
    return () => {
      mounted.current = false
    }
  }, [loadData])

  const removeAll = (recipeId: string) => {
    removeAllFromCart(cart.filter(item => item.recipeId === recipeId))
  }

  if (!user) {
    history.push("/login")
  }

  if (cart.length < 1) {
    return <NoResultsText>Nothing on the shopping list</NoResultsText>
  }

  if (loading || recipes == null) {
    return <Loading/>
  }

  return (
    <Grid container spacing={2} justify='center' className={classes.container}>
      {recipes.map((recipe) =>
        <Grid item xs={12} sm={9} md={6} lg={4} xl={3} key={recipe.id}>
          <Paper>
            <Table className={classes.table}>
              <TableHead>
                <TableRow className={classes.tableHeader}>
                  <TableCell className={classes.headerCellTitle} align='center' colSpan={3}>
                    <Link className={classes.link} to={'/recipe/' + recipe.id}>{recipe.name}</Link>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableHead>
                <TableRow className={classes.tableHeader}>
                  <TableCell className={classes.headerCellMeasure} align='right'>Quantity</TableCell>
                  <TableCell className={classes.headerCellIngredient}>Ingredient</TableCell>
                  <TableCell className={classes.headerCellActions} align='center'>
                    <IconButton onClick={() => removeAll(recipe.id)}
                                className={classes.shoppingCartIcon}><RemoveShoppingCart fontSize='small'/></IconButton>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {
                  cart.filter(item => item.recipeId === recipe.id).map((row, index) =>
                    <TableRow key={index}>
                      <TableCell className={classes.measureCell} align='right'>{row.ingredient.measurement}</TableCell>
                      <TableCell className={classes.ingredientCell}>{row.ingredient.ingredient}</TableCell>
                      <TableCell className={classes.actionCell} align='center'>
                        <CartButton ingredient={row.ingredient} recipeId={recipe.id}/>
                      </TableCell>
                    </TableRow>
                  )
                }
              </TableBody>
            </Table>
          </Paper>
        </Grid>
      )}
    </Grid>
  )
}