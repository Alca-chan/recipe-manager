import React, {useContext, useState} from 'react';
import {LoginRegistrationForm} from "../components/LoginRegistrationForm";
import {Redirect} from "react-router-dom";
import {Card, Container, Tab, Tabs} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {UserContext} from "../providers/UserProvider";

export type action = 'Login' | 'Register'

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: theme.spacing(2)
  },
  header: {
    background: theme.palette.secondary.main
  },
  indicator: {
    backgroundColor: theme.palette.text.primary
  }
}))

export const Login = () => {
  const [tabIndex, setTabIndex] = useState(1)

  const classes = useStyles()

  const {user} = useContext(UserContext)

  if (user) {
    return <Redirect to='/'/>
  }

  const handleTabChange = (event: React.ChangeEvent<{}>, newTabIndex: number) => {
    setTabIndex(newTabIndex)
  }

  return (
    <Container maxWidth='sm'>
      <Card className={classes.card}>
        <Tabs className={classes.header} classes={{indicator: classes.indicator}} value={tabIndex}
              onChange={handleTabChange} centered>
          <Tab value={1} label="Login" {...a11yProps(1)} />
          <Tab value={2} label="Register" {...a11yProps(2)} />
        </Tabs>
        <LoginRegistrationForm actionName={tabIndex === 1 ? 'Login' : 'Register'}/>
      </Card>
    </Container>
  )
}

function a11yProps(index: number) {
  return {
    id: `tab-${index}`,
    'aria-controls': `tabpanel-${index}`,
  }
}