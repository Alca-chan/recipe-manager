import React, {useCallback, useContext, useEffect, useRef, useState} from 'react';
import {getRecipeById} from "../api/api";
import {FullRecipe} from "../api/types";
import {Container, fade, Grid, IconButton} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {IngredientsTable} from "../components/Recipe/IngredientsTable";
import {ImageView} from "../components/RecipesSearch/ImageView";
import {Instructions} from "../components/Recipe/Instructions";
import {NoResultsText} from "../components/NoResultsText";
import {Title} from "../components/Recipe/Title";
import {UserContext} from "../providers/UserProvider";
import {recipeCollection} from "../utils/firebase";
import {useHistory} from "react-router-dom";
import {FavoritesContext} from "../providers/FavoritesProvider";
import red from "@material-ui/core/colors/red";
import {Edit, Favorite} from "@material-ui/icons";
import {Loading} from "../components/Loading";

const useStyles = makeStyles(theme => ({
  container: {
    padding: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  imageContainer: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(4),
    width: '100%',
    justifyContent: 'center',
    display: 'flex',
    alignItems: 'center'
  },
  image: {
    maxWidth: '700px'
  },
  like: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      color: red[400]
    }
  },
  likeSelected: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    backgroundColor: fade(red[100], 0.80),
    color: red[400],
    '&:hover': {
      backgroundColor: fade(red[100], 0.80),
      color: theme.palette.text.secondary,
    }
  }
}))

export const RecipePage = (props: { match: any }) => {
  const [loading, setLoading] = useState(false)
  const mounted = useRef(true)
  const [data, setData] = useState<FullRecipe | null>()
  const [error, setError] = useState(false)
  const {user} = useContext(UserContext)
  const {favorites, addToFavorites} = useContext(FavoritesContext)
  const history = useHistory()

  const classes = useStyles()

  const id: string = props.match.params.id

  const loadRecipeFromApi = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      let recipes = await getRecipeById(id)
      if (recipes.length < 1) {
        if (!mounted.current) return
        setData(undefined)
        setError(true)
      } else {
        if (mounted.current) setData(recipes[0])
      }
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [id, setLoading])

  const loadRecipeFromFirebase = useCallback(async () => {
    if (mounted.current) setLoading(true)
    try {
      if (!mounted) return
      if (!user) {
        setData(undefined)
        setError(true)
        return
      }
      await recipeCollection
        .where("userId", "==", user.uid)
        .where("id", "==", id)
        .get()
        .then((querySnapshot) => {
          if (!mounted.current) return
          const docs = querySnapshot.docs
          if (docs.length !== 1) {
            setData(undefined)
            setError(true)
            return
          }
          const recipe = docs[0].data() as FullRecipe
          if (mounted) setData(recipe)
        })
    } finally {
      if (mounted.current) setLoading(false)
    }
  }, [user, id, setLoading])

  useEffect(() => {
    mounted.current = true
    if (!id.includes("-")) {
      loadRecipeFromApi()
    } else {
      loadRecipeFromFirebase()
    }
    return () => {
      mounted.current = false
    }
  }, [id, loadRecipeFromApi, loadRecipeFromFirebase])

  const edit = () => {
    history.push("/editrecipe/" + data?.id)
  }

  if (error) {
    return (
      <NoResultsText>No recipe with id {id} found</NoResultsText>
    )
  }

  if (loading || data == null) {
    return <Loading/>
  }

  return (
    <Container className={classes.container}>
      <Title title={data.name}/>

      {
        user && (
          <div>
            <IconButton onClick={edit} className={classes.like}>
              <Edit/>
            </IconButton>
            <IconButton onClick={() => addToFavorites(id)}
                        className={favorites?.some(like => like === id) ? classes.likeSelected : classes.like}>
              <Favorite/>
            </IconButton>
          </div>
        )
      }
      <div className={classes.imageContainer}>
        <div className={classes.image}>
          <ImageView imgUrl={data.imgUrl} width='100%'/>
        </div>
      </div>
      <Grid container spacing={4} justify='space-between'>
        <Grid item xs={12} md={6}>
          <IngredientsTable data={data.ingredients} recipeId={data.id}/>
        </Grid>
        <Grid item xs={12} md={6}>
          <Instructions data={data.instructions}/>
        </Grid>
      </Grid>
    </Container>
  )
}